﻿#include <iostream>

int main()
{
    int const a = 100;
    for (int i = 0; i < a; i++)
    {
        if (i % 2 == 1)
        {
            std::cout << i << "\n";
        }
    }
    
}
